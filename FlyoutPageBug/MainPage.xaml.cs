﻿using CommunityToolkit.Maui.Views;

namespace FlyoutPageBug;

public partial class MainPage : FlyoutPage
{
	public MainPage()
	{
		InitializeComponent();
	}

    private async void Button_OnClicked(object sender, EventArgs e)
    {
        await this.ShowPopupAsync(new TestPopup());
    }
}


