﻿namespace FlyoutPageBug;

public partial class AppShell : ContentPage
{
	public AppShell()
	{
		InitializeComponent();
	}

    async void Button_Clicked(System.Object sender, System.EventArgs e)
    {
        await App.Current.MainPage.Navigation.PushModalAsync(new MainPage());
    }
}

